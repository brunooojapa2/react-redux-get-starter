import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Card,
  CardItem,
  Drawer
} from 'native-base';

import AppSidebar from './components/AppSideBar';
import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';
import AppCard_image from './components/AppCard_image';

export default class app extends Component {
  closeDrawer = () => {
    this.drawer._root.close();
  };
  openDrawer = () => {
    this.drawer._root.open();
  };
  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={<AppSidebar />}
        onClose={() => this.closeDrawer()}
      >
        <AppHeader openDrawer={this.openDrawer.bind(this)} />
        <AppCard_image />
        <AppFooter />
      </Drawer>
    );
  }
}
