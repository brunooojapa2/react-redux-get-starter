import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right
} from 'native-base';
export default class AppCard_image extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                  }}
                />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{
                  uri:
                    'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                  }}
                />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{
                  uri:
                    'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                  }}
                />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{
                  uri:
                    'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                  }}
                />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{
                  uri:
                    'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                  }}
                />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{
                  uri:
                    'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
                }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

module.exports = AppCard_image;
