import React, { Component } from 'react';
import { Text, Image } from 'react-native';

import { Content, Separator, ListItem, Icon } from 'native-base';

export default class AppSideBar extends Component {
  render() {
    return (
      <Content style={{ backgroundColor: '#FFFFFF' }}>
        <Image
          source={{
            uri:
              'https://www.clickgratis.com.br/fotos-imagens/paisagens/aHR0cDovL2ZvbGhhbm9icmUueHl6LzIwMTUvMDkvUGFpc2FnZW1faW1hZ2VtX2xpbmRhXy01Mi5qcGc=.jpg'
          }}
          style={{ height: 200, width: null, flex: 1 }}
        />
        <Separator bordered>
          <Text>Separator</Text>
        </Separator>
        <ListItem>
          <Text>Joseteste</Text>
          <Icon name="calendar" />
        </ListItem>
        <ListItem>
          <Text> Joseteste</Text>
        </ListItem>
        <ListItem last>
          <Text>Kelso Joseteste</Text>
        </ListItem>
        <Separator bordered>
          <Text>Separator</Text>
        </Separator>
        <ListItem>
          <Text>Caroline Joseteste</Text>
        </ListItem>
      </Content>
    );
  }
}

module.exports = AppSideBar;
